# Mutant Challenge

Desafio da Mutant feito usando [NodeJS](https://nodejs.org/en/), [Express](https://expressjs.com/), [ElasticSearch](https://www.elastic.co/) e [Typescript](https://www.typescriptlang.org/).

# Instalação

Existem duas mameiras para rodar o projeto:

# Vagrant

É necessário a instalação do [VirtualBox](https://www.virtualbox.org/wiki/Downloads) e do [Vagrant](https://www.vagrantup.com/downloads.html).

Rodando dentro da raiz do projeto o comando `vagrant up`, será feito a instalação de tudo que é necessário para rodar o projeto.

```bash
vagrant up
```

# Estrutura de teste

O projeto é uma simples API que roda no endereço `http://55.55.55.5:8080`. Os seguintes endpoints são disponibilizados da seguinte maneira:

`GET /websites`: retorna uma lista de websites

`GET /usersInOrder`: retorna um objeto de usuários ordenado pelo nome

`GET /usersSuite`: retorna um objeto de usuários que possuem 'suite'

O projeto salva todos os logs a cada GET no ElastiSearch. Para acessar esses logs basta acessar o endereço `http://55.55.55.5:9200/user`. 

# Manualmente utilizando Docker

É necessário a instalação do [Docker](https://docs.docker.com/install/) e do [Docker Compose](https://docs.docker.com/compose/install/) para construir os containers do NodeJS e do ElasticSearch.

Se o seu sistema tem suporte para Bash, então basta rodar dentro do projeto o comando `run.sh` para iniciar a instalação e configuração dos containers. Você pode rodar:

```bash
./run.sh 

# OU

sh run.sh
```

> Talvez não tenha permissão para rodar o comando acima, então rode antes `chmod +x run.sh` para dar permissão.

O script `run.sh` nos dá a permissão de rodar os comandos que farão a configuração do ambiente Node. Por exemplo, instalação de dependencias.

Se o seu sistema não tem suporte ao Bash, então rode manuamente os comandos:

```bash
# Instalação de dependencias
docker run --rm -v $(pwd):/app -w /app -it node:10.15.3-alpine npm install

# Contruindo e rodando o projeto
docker-compose up -d --build

```

# Estrutura de teste

O projeto é uma simples API que roda no endereço `http://localhost:8080`. Os seguintes endpoints são disponibilizados da seguinte maneira:

`GET /websites`: retorna uma lista de websites

`GET /usersInOrder`: retorna um objeto de usuários ordenado pelo nome

`GET /usersSuite`: retorna um objeto de usuários que possuem 'suite'

O projeto salva todos os logs a cada GET no ElastiSearch. Para acessar esses logs basta acessar o endereço `http://localhost:9200/user`
