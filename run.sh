#!/bin/bash

echo "Inicializando o servidor de desenvolvimento..."
docker-compose up -d --build
echo "Finalizado!"
echo "Instalando dependencias..."
docker run --rm -v $(pwd):/app -w /app -it node:10.15.3-alpine npm install
echo "Finalizado!"