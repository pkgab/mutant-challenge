export const environment = {
  server: { port: process.env.SERVER_PORT || 8080 },
  endPointUsers: { url: process.env.URL_USERS || 'http://jsonplaceholder.typicode.com/users'},
  endPointElasticSearch: { url: process.env.URL_ELASTICSEARCH || 'http://localhost:9200'}
}