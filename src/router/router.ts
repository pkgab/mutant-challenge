import * as express from 'express'
import { users } from '../users/users'
import { userService } from '../services/user.service'
import { elasticSearch } from '../services/elasticsearch.service'

class Router {

  private getAllWebsites = (req: express.Request, res: express.Response) => {
    users.getUsers().then(users => {
      const webSites = userService.filterWebSites(users)
      elasticSearch.saveDocumentInUserIndex(Object.assign({}, webSites)).then(() => {
        res.json({websites: webSites})
      }).catch(error => console.error(error))
    }).catch(error => console.error(error))
  }

  private getUsersInOrder = (req: express.Request, res: express.Response) => {
    users.getUsers().then(users => {
      const orderedUsers = userService.filterUsersInOrder(users)
      elasticSearch.saveDocumentInUserIndex(orderedUsers).then(() => {
        res.json({orderedusers: orderedUsers})
      }).catch(error => console.error(error))
    }).catch(error => console.error(error))
  }

  private getUsersSuite = (req: express.Request, res: express.Response) => {
    users.getUsers().then(users => {
      const usersSuite = userService.filterSuiteAddress(users)
      elasticSearch.saveDocumentInUserIndex(usersSuite).then(() => {
        res.json({userssuite: usersSuite})
      }).catch(error => console.error(error))
    }).catch(error => console.error(error))
  }

  public applyRoutes(application: express.Application) {
    application.get('/websites', this.getAllWebsites)
    application.get('/usersInOrder', this.getUsersInOrder)
    application.get('/usersSuite', this.getUsersSuite)
  }
}

export const router =  new Router()