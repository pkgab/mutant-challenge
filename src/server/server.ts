import * as express from 'express'
import { environment } from '../common/environment'
import { router } from '../router/router'
import { elasticSearch } from '../services/elasticsearch.service'

export class Server {

  public application: express.Application

  private initConfig(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.application = express()
        router.applyRoutes(this.application)

        this.application.listen(environment.server.port, () => {
          resolve(this.application)
        })
      } catch(error) {
        reject(error)
      }
    })
  }

  public bootstrap(): Promise<Server> {
    return elasticSearch.createClient().then(() => 
          this.initConfig().then(() => this))
  }
}