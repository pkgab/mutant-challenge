import * as elasticsearch from 'elasticsearch'
import { environment } from '../common/environment'

class ElasticSearchService {

  private client: elasticsearch.Client

  public createClient(): Promise<elasticsearch.Client> {
    return new Promise((resolve, reject) => {
      try {
        this.client = new elasticsearch.Client({
          host: environment.endPointElasticSearch.url
        })
        resolve(this.client)
      } catch(error) {
        reject(error)
      }
    })
  }

  public async createUserIndex(): Promise<any> {
    return this.client.indices.create({
      index: 'user'
    },(err, resp, status) => {
      if(err) {
        console.error(err);
      }
      else {
        console.log(status)
        console.log("create", resp);
      }
    })
  }

  public async saveDocumentInUserIndex(body) {
    const userIndex = await this.isUserIndex()

    if (typeof userIndex.status !== 'undefined' && userIndex.status == 404) {
      await this.createUserIndex()
    }

    return await this.client.index({  
      index: 'user',
      type: 'get',
      body: {body}
    });
  }

  private async isUserIndex(): Promise<any> {
    return await this.client.search({
      index: 'user',
    }).then(result => result)
    .catch(error => error) 
  }
}

export const elasticSearch = new ElasticSearchService()